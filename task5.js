function range(a,b,c) {
    var str = [];
 
    if (c === undefined) {
      if (a < b) {
        c = 1;
      } else {
        c = -1;
      }
    }
  
    if (a > b) {
      for (a; a >= b; a += c) {
        str.push(a);
      }
    } else {
      for (a; a <= b; a += c) {
        str.push(a);
      }
    }
    return str;
  }
  
  function sum(arr) {
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
      sum += arr[i];
    }
    return sum;
  }
  