function isEven(number) {
  if ((typeof(number) !== 'number') || (parseInt(number, 10) !== number)) {
    return null;
  }
  var even = 0;
  var odd = 1;
  if (number < 0) {
    number *= -1;
  }
  if (number === even) {
    return true;
  } else if (number === odd) {
    return false;
  } else {
    return isEven(number - 2);
  }
}
